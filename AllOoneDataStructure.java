import java.util.*;

class AllOne {
    private Map<String, Integer> countMap;
    private TreeMap<Integer, Set<String>> keyCountMap;

    public AllOne() {
        countMap = new HashMap<>();
        keyCountMap = new TreeMap<>();
    }
    
    public void inc(String key) {
        int count = countMap.getOrDefault(key, 0);
        countMap.put(key, count + 1);
        
        if (!keyCountMap.containsKey(count + 1))
            keyCountMap.put(count + 1, new HashSet<>());
        keyCountMap.get(count + 1).add(key);
        
        if (count > 0) {
            keyCountMap.get(count).remove(key);
            if (keyCountMap.get(count).isEmpty())
                keyCountMap.remove(count);
        }
    }
    
    public void dec(String key) {
        if (!countMap.containsKey(key)) return;
        
        int count = countMap.get(key);
        if (count == 1) {
            countMap.remove(key);
        } else {
            countMap.put(key, count - 1);
        }
        
        if (!keyCountMap.containsKey(count)) return;
        
        keyCountMap.get(count).remove(key);
        if (keyCountMap.get(count).isEmpty())
            keyCountMap.remove(count);
        
        if (count > 1) {
            if (!keyCountMap.containsKey(count - 1))
                keyCountMap.put(count - 1, new HashSet<>());
            keyCountMap.get(count - 1).add(key);
        }
    }
    
    public String getMaxKey() {
        if (keyCountMap.isEmpty()) return "";
        return keyCountMap.lastEntry().getValue().iterator().next();
    }
    
    public String getMinKey() {
        if (keyCountMap.isEmpty()) return "";
        return keyCountMap.firstEntry().getValue().iterator().next();
    }
}

/**
 * Your AllOne object will be instantiated and called as such:
 * AllOne obj = new AllOne();
 * obj.inc(key);
 * obj.dec(key);
 * String param_3 = obj.getMaxKey();
 * String param_4 = obj.getMinKey();
 */
